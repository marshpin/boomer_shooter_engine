extern crate sdl2;

use gl::types::GLsizei;
use sdl2::video::Window;
use sdl2::VideoSubsystem;

pub fn init_window(video_sub: &VideoSubsystem, title: &str, width: u32, height: u32) -> Window {
    let window = video_sub
        .window(title, width, height)
        .opengl()
        .build()
        .unwrap();

    return window;
}