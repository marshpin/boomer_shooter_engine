extern crate sdl2;
extern crate gl;

use gl::types::GLsizei;

pub mod utils;

const WINDOW_WIDTH: u32 = 1280;
const WINDOW_HEIGHT: u32 = 720;

fn main() {


    let context = sdl2::init().unwrap();
    let video_sub = context.video().unwrap();
    let gl_attr = video_sub.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 5);
    let window = utils::init_window(&video_sub, "SDL Window", WINDOW_WIDTH, WINDOW_HEIGHT);
    let _gl_context = window.gl_create_context().unwrap();
    let _gl = gl::load_with(|s| video_sub.gl_get_proc_address(s) as *const std::os::raw::c_void);

    unsafe {
        gl::Viewport(0, 0, WINDOW_WIDTH as GLsizei, WINDOW_HEIGHT as GLsizei);
        gl::ClearColor(0.3,0.3,0.5,1.0);
    }

    let mut event_pump = context.event_pump().unwrap();
    'window: loop {
        for event in event_pump.poll_iter() {
            use sdl2::event::Event;

            match event {
                Event::Quit { .. } => break 'window,
                _ => {},
            }
        }

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }

        window.gl_swap_window();
    }
}
